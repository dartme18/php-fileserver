from archlinux/base
run pacman -Syu --noconfirm reflector && reflector --verbose --latest 15 --sort rate --save /etc/pacman.d/mirrorlist
run ln -sf /usr/share/zoneinfo/America/Indianapolis /etc/localtime
entrypoint ["/startup_script"]
expose 80
run pacman -S --noconfirm apache php-apache php-sqlite;
copy httpd.conf /etc/httpd/conf/httpd.conf
copy php /srv/http/fileserver/
copy startup_script /
