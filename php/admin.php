<?php
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
}
require('include/settings.php');
require('include/file_handling.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $forward = null;
    if (isset($_FILES) && isset($_FILES['uploads'])) {
        $forward = create_download_link();
    } else if (isset($_POST['create-upload-link'])) {
        $forward = create_upload_link($_POST['number-of-files']);
    }
    if ($forward !== 'admin.php') {
        header("Location: " . $forward);
        exit (0);
    }
}
$downloads = get_available_downloads();
$uploads = get_available_uploads();
?>

<html>
    <head>
        <title>PHP File Server</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="parent">
            <h1>
                Administration
            </h1>
            <div class="used-uploads section">
                <h2>
                    Available Upload Links
                </h2>
                <table>
                    <thead>
                        <tr>
                            <th>Delete</th>
                            <th>URL</th>
                            <th>Files Allowed</th>
                            <th>Available</th>
                            <th>Download</th>
                            <th>Note</th>
                        </tr>
                    </thead>
                    <tbody>
                        <script type="text/javascript">function available_upload_selected() {
                            el = document.getElementById('available-upload-select');
                            if (!el) return;
                            option = el[el.selectedIndex].value;
                            if (option === 'dummy') return;
                            window.location.replace('get_available_upload.php?key=' + option);
                        }</script>

<?php
function output_upload_link($upload, $i) {
    echo ('<td>');
    echo ('<a class="delete-upload-file-link" href="deleteuploadfile.php?uploadKey=' . $upload['available_files'][$i]['rowid'] . '">✖</a>');
    echo (' <span class="available-file">');
    echo ('<a href="get_available_upload.php?key=' . $upload['key'] . '.' . $upload['available_files'][$i]['rowid'] . '">');
    echo (htmlspecialchars($upload['available_files'][$i]['filename']) . '</a>');
    echo ('</div></td><td class="available-upload-note">');
    echo (htmlspecialchars($upload['available_files'][$i]['note']));
    echo ('</td>');
}
foreach ($uploads as $upload) {
    echo ('<tr><td><a href="deleteupload.php?uploadKey=' . $upload['key'] . '">✖</a></td>');
    echo ('<td><a href="' . $base_url . 'upload/' . $upload['key'] . '">Upload Link</a></td>');
    echo ('<td>' . $upload['allowed_files'] . '</td>');
    echo ('<td>' . count($upload['available_files']) . '</td>');
    if (count($upload['available_files']) > 0) {
        output_upload_link($upload, 0);
    }
    echo ('</tr>');
    for ($i = 1; $i < count($upload['available_files']); ++$i) {
        echo ('<tr><td></td><td></td><td></td><td></td>');
        output_upload_link($upload, $i);
        echo ('</tr>');
    }
}
?>
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="download-links section">
                <h2>
                    Available Downloads
                </h2>
                <table>
                    <thead>
                        <tr>
                            <th>Delete</th>
                            <th>URL</th>
                            <th>Filename</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
foreach ($downloads as $download) {
    echo ('<tr><td><a href="deletedownload.php?downloadKey=' . $download['key'] . '">✖</a></td>');
    echo ('<td><a href="' . $base_url . 'downloads/' . $download['key'] . '">Download Link</a></td>');
    echo ('<td>' . htmlspecialchars($download['filename']) . '</td></tr>');
}
?>
                    </tbody>
                </table>
            </div>
            <hr>
            <form class="section upload-body" action="admin.php" enctype="multipart/form-data" method="POST">
                <h2>
                    Create New Upload Link
                </h2>
                <label class="breathe" for="number-of-files" alt="Enter 0 for unlimited files">Number of files to accept:</label>
                <input class="breathe" type="number" step="1" min="0" max="10000" name="number-of-files" />
                <input class="breathe" type="submit" name="create-upload-link" value="Create New Link" />
            </form>
            <hr>
            <div class="new-download section upload-body">
                <h2>
                    Create New Download Link
                </h2>
                <form class="breathe" enctype="multipart/form-data" action="admin.php" method="POST">
                    <!-- 10 gigs max -->
                    <input type="hidden" name="MAX_FILE_SIZE" value="10000000000" />
                    <span class="breathe">File to Upload:</span>
                    <input class="breathe" name="uploads[]" type="file" multiple/>
                    <input class="breathe" type="submit" value="Upload Files" />
                </form>
            </div>
        </div>
    </body>
</html>
