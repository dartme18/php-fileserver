<?php
#no auth; if you have the link, you can act.
require('include/file_handling.php');
require('include/settings.php');

$can_upload = can_upload($_GET['key']);
function process_request() {
    global $max_guest_upload_size;
    global $can_upload;
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        error_log("upload.php: request is not a post.");
        return;
    }
    error_log("upload.php: post");
    if ($can_upload !== true) {
        error_log("upload.php: Attempt to upload to key " . $_GET['key'] . " failed. can_upload is $can_upload.");
        return;
    }
    error_log("upload.php: can upload is true.");
    if (!isset($_FILES['upload'])) {
        error_log("upload.php: No file was uploaded to upload.php with key " . $_GET['key']);
        $can_upload = "No file sent";
        return;
    }
    $error_code = upload_error_code_to_string($_FILES['upload']['error']);
    if ($error_code !== null) {
        error_log("upload.php: some other error: " . var_export($error_code, true));
        $can_upload = $error_code;
        return;
    }
    if ($_FILES['upload']['size'] > $max_guest_upload_size) {
        $can_upload = 'Upload too large.';
        return;
    }
    $name = $_FILES['upload']['name'];
    $tmp_file = $_FILES['upload']['tmp_name'];
    $note = $_POST['note'];
    $return = store_upload($_GET['key'], $name, $tmp_file, $note);
    if ($return === true) {
        $can_upload = 'Upload successful';
    } else {
        $can_upload = $return;
    }
}
process_request();
?>

<html>
<head>
    <title>File Server - Upload</title>
    <link rel="stylesheet" href="../style.css"/>
</head>
<body>
<?php
if ($can_upload === true) {
    error_log("max upload size is $max_guest_upload_size");
    ?>
    <div>
        <form class="upload-form" action=<?= '"' . $_GET['key'] . '"' ?> enctype="multipart/form-data" method="POST">
            <h1>File Server Upload</h1>
            <!-- For the user's benefit, the browser can warn him about the upload size limit. -->
            <input type="hidden" name="MAX_FILE_SIZE" value="<?= $max_guest_upload_size ?>" />
            <label class="upload-label" for="upload">File to Upload:</label>
            <input class="upload-upload" id="upload" name="upload" type="file" />
            <label class="upload-note-label" for="note">Note to Administrator:</label>
            <textarea class="upload-note-input" maxlength="1000" id="note"name="note"></textarea>
            <input class="upload-button" type="submit" value="Upload File" />
        </form>
    </div>
    <?php
} else if ($can_upload === "Upload successful") {
    echo("<div>Upload successful.</div>");
} else if ($can_upload === "Too many uploads") {
    echo("<div>All the uploads for this key have been submitted.</div>");
} else if ($can_upload === "Key not recognized") {
    echo("<div>Uploads have not been authorized for that key.</div>");
} else if ($can_upload === null ) {
    error_log("Error checking can_upload with hash " . $_GET['key']);
    echo ("<div>Internal Error</div>");
} else {
    echo ("<div>$can_upload</div>");
}?>

</body>
</html>
