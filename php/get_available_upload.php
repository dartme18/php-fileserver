<?php
require("include/auth.php");
if (!is_authenticated()) {
    show_auth_page();
    exit(1);
}
require('include/file_handling.php');
if (serve_available_upload($_GET['key'])) {
    exit (0);
}
error_log("Error getting available upload with key " . $_GET['key']);
?>

<html><head><title>File Server</title></head>
<body>
<div>No upload with that key.</div>
</body></html>

