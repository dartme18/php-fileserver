<?php
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
    exit (0);
}
require('include/file_handling.php');
if (delete_one_upload($_GET['uploadKey'])) {
    header('Location: admin.php');
    exit (0);
}
?>
<html><head><title>PHP File Transfer</title></head>
<body>
<div>
No upload with that key exists.
</div>
</body>
</html>

