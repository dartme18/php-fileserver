<?php

# Exported to other modules
$base_url = getenv("BASE_URL");

/* Creates the download log table if necessary. */
function download_log_table($db) {
	$ret = $db->exec("select 1 from download_log;");
	if ($ret === false) {
		error_log("download_log table doens't exist. Creating...");
		$ret = $db->exec("create table download_log(hash varchar, ipaddr varchar, timestamp datetime default current_timestamp);");
		error_log("Return from creating download_log is " . var_export($ret, true));
	}
}

function upload_note_column($db) {
    $table_info = $db->query("pragma table_info(uploads)");
    foreach ($table_info as $info) {
        if ($info['name'] === 'note') {
            return;
        }
    }
    error_log("upload table does not have note column. Adding it.");
    $result = $db->query("alter table uploads add note varchar;");
    error_log("Result of altering uploads table is " . var_export($result, true));
}

function get_connection() {
    $sqlite_file = '/data/fileserver.db';
    $create_tables = !file_exists($sqlite_file);
    try {
        $db = new PDO("sqlite:$sqlite_file");
    } catch (Exception $e) {
        error_log("Got exception connecting: " . var_export($e, true));
        error_log("db is " . var_export($db, true));
    }
    if ($create_tables) {
        create_tables($db);
    }
    download_log_table($db);
    upload_note_column($db);
    return $db;
}

function create_tables($db) {
    $db->exec("create table version(version varchar not null);");
    $db->exec("insert into version (version) values ('1.0');");
    $db->exec("create table upload_link (hash varchar unique,
        num_files integer);");
    $db->exec("create table uploads (upload_id varchar, filename varchar);");
    $db->exec("create table downloads (hash varchar unique, filename varchar);");
}

/**
 * Returns something in the shape:
 * { key : 'acabce1cb312',
 *   allowed_files : 5,
 *   available_files : [ 'filename1', 'filename2', 'filename3' ]
 * }
 * Returns null upon internal error.
 */
function get_available_uploads() {
    error_log("Getting uploads");
    if (null === ($db = get_connection())) {
        return null;
    }
    $ret = array();
    foreach ($db->query('select l.hash, l.num_files from upload_link l;') as $link_result) {
        $uploads = array();
        $st = $db->prepare('select filename, rowid, note from uploads where upload_id = ?;');
        error_log("st is " . var_export($st, true));
        if (!$st->execute(array($link_result['hash']))) {
            error_log("unable to execute query for key " . $link_result['hash']);
            continue;
        }
        error_log("Getting uploads for upload link with hash " . $link_result['hash']);
        foreach ($st->fetchAll() as $upload_result) {
            error_log("  got child, " . $upload_result['filename']);
            array_push($uploads, array('filename' => $upload_result['filename'],
                'rowid' => $upload_result['rowid'],
                'note' => $upload_result['note']));
        }
        array_push($ret, array('key'=>$link_result['hash'], 'allowed_files'=>$link_result['num_files'],
            'available_files' => $uploads));
    }
    return $ret;
}

/*
 * Returns true or a string explaining why not.
 * Returns null on internal error.
 */
function can_upload($key) {
    if (null === ($db = get_connection())) {
        return null;
    }
    $st = $db->prepare("select num_files, rowid from upload_link where hash = ?;");
    if (!$st->execute(array($key))) {
        return 'Internal Error';
    }
    $allowed = $st->fetchAll();
    if (count($allowed) == 0){
        return 'Key not recognized';
    }
    $allowed = $allowed[0];
    $st = $db->prepare("select count(*) from uploads where upload_id = ?;");
    $have = $st->execute(array($allowed['rowid']));
    if ($have['count'] < $allowed['num_files']) {
        return true;
    }
    return 'Too many uploads';
}

function create_upload_link($number_of_files) {
    error_log("Creating upload link");
    if (null === ($db = get_connection())) {
        return null;
    }
    $forward = 'admin.php';
    $key = uniqid();
    $st = $db->prepare("insert into upload_link (hash, num_files) values (?,?);");
    if (!$st->execute(array($key, $number_of_files))) {
        error_log("Error creating upload link key $key number of files $number_of_files");
        return $forward . '?error=' . urlencode('Internal error executing SQL.');
    }
    return $forward;
}

function serve_available_upload($key) {
    error_log("Serving available upload, $key");
    if (null === ($db = get_connection())) {
        return false;
    }
    $exploded = explode(".", $key);
    if (count($exploded) < 2) {
        error_log("Exploded count is " . count($exploded) . " failing serve available upload.");
        return false;
    }
    $st = $db->prepare("select filename from uploads where upload_id = ? and rowid = ?;");
    if (!$st->execute($exploded)) {
        error_log("Unable to find upload with key $key.");
        return false;
    }
    $res = $st->fetchAll();
    $sought_file = '/data/' . $exploded[0] . '-upload-' . $exploded[1];;
    if (!file_exists($sought_file)) {
        error_log("file $sought_file wasn't found. unable to serve available upload.");
        return false;
    }
    $download_file_name = urlencode($res[0]['filename']);
    error_log("Downloading $download_file_name from key $key ($exploded[1] element)");
    header('Content-Type: application/octet-stream');
    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename="'.$download_file_name.'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($sought_file));
    readfile($sought_file);
    return true;
}

function delete_upload_link($key) {
    error_log("Deleting upload link for key $key");
    $ret = true;
    if (null === ($db = get_connection())) {
        return false;
    }
    $st = $db->prepare("select rowid from uploads where upload_id = ?;");
    if (!$st->execute(array($key))) {
        error_log("Unable to search for uploads for that key.");
        return false;
    }
    $sought_files = array();
    foreach ($st->fetchAll() as $upload) {
        $filename = "/data/$key-upload-" . $upload['rowid'];
        if (!file_exists($filename)) {
            error_log("$filename doesn't exist. continuing.");
            $ret = false;
        }
        error_log("deleting file $filename");
        if (!unlink($filename)) {
            error_log("Error deleting $filename. continuing.");
            $ret = false;
        }
    }
    $st = $db->prepare("delete from uploads where upload_id = ?;");
    if (!$st->execute(array($key))) {
        error_log("error deleting uploads from database. Continuing!");
        $ret = false;
    }
    $st = $db->prepare("delete from upload_link where hash = ?;");
    if (!$st->execute(array($key))) {
        error_log("Error deleting upload link. continuing.");
        $ret = false;
    }
    return $ret;
}

function get_available_downloads() {
    error_log("Getting downloads");
    if (null === ($db = get_connection())) {
        return null;
    }
    $ret = array();
    $result = $db->query('select hash, filename from downloads;');
    error_log("Getting downloads: " . var_export($result, true));
    foreach ($db->query('select hash, filename from downloads;') as $link_result) {
        array_push($ret, array('key'=>$link_result['hash'], 'filename' => $link_result['filename']));
    }
    return $ret;
}

function store_available_download($filename, $tempfile) {
    error_log("storing available download, $filename, from $tempfile");
    if (null === ($db = get_connection())) {
        return null;
    }
    if (!is_uploaded_file($tempfile)) {
        error_log("filename $filename tempfile $tempfile is not uploaded?");
        return 'Why u trick me?';
    }
    $newfile = "/data/$filename";
    if (file_exists($newfile)) {
        return "Already have $filename";
    }
    if (!move_uploaded_file($tempfile, $newfile)) {
        return 'Internal Error Saving';
    }
    $hash = hash('md5', $filename);
    $st = $db->prepare("insert into downloads (hash, filename) values (?,?);");
    if (!$st->execute(array($hash, $filename))) {
        unlink ($newfile);
        error_log("Error executing query; unlinked new file, $newfile");
        return 'Internal Error Writing';
    }
    return null;
}

function create_download_link() {
    $forward = 'admin.php';
    for ($i = 0; $i < count($_FILES['uploads']['name']); ++$i) {
        $error = upload_error_code_to_string($_FILES['uploads']['error'][$i]);
        if ($error !== null) {
            $forward .= '?error=' . urlencode($error);
            break;
        }
        $filename = urldecode($_FILES['uploads']['name'][$i]);
        $tmp_file = $_FILES['uploads']['tmp_name'][$i];
        $error = store_available_download(urldecode($filename), $tmp_file);
        if ($error !== null) {
            error_log("create-download-link got error $error");
            $forward .= '?error=' . urlencode($error);
            break;
        }
    }
    return $forward;
}

function upload_error_code_to_string($code)
{
    switch ($code) {
    case UPLOAD_ERR_OK: return null;
    case UPLOAD_ERR_INI_SIZE: return "The uploaded file exceeds the upload_max_filesize directive in php.ini";
    case UPLOAD_ERR_FORM_SIZE: return "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
    case UPLOAD_ERR_PARTIAL: return "The uploaded file was only partially uploaded";
    case UPLOAD_ERR_NO_FILE: return "No file was uploaded";
    case UPLOAD_ERR_NO_TMP_DIR: return "Missing a temporary folder";
    case UPLOAD_ERR_CANT_WRITE: return "Failed to write file to disk";
    case UPLOAD_ERR_EXTENSION: return "File upload stopped by extension";
    default: return "Unknown Error";
    }
}

function delete_one_upload($rowid) {
    error_log("Deleting upload file for rowid $rowid");
    if (null === ($db = get_connection())) {
        return false;
    }
    $st = $db->prepare("select upload_id from uploads where rowid = ?;");
    if (!$st->execute(array($rowid))) {
        error_log("Unable to find upload for that rowid.");
        return false;
    }
    $res = $st->fetchAll();
    error_log("got this from fetchall: " . var_export($res, true));
    if (count($res) === 0) {
        error_log("Unable to fetch upload for that rowid.");
        return false;
    }
    $upload_hash = $res[0]['upload_id'];
    $filename = "/data/$upload_hash-upload-" . $rowid;
    if (!file_exists($filename)) {
        error_log("$filename doesn't exist.");
        return false;
    }
    error_log("deleting file $filename");
    if (!unlink($filename)) {
        error_log("Error deleting $filename. continuing.");
        /* We don't want to remove the database record because there may be a dangling file. */
        return false;
    }
    $st = $db->prepare("delete from uploads where rowid = ?;");
    if (!$st->execute(array($rowid))) {
        error_log("error deleting uploads from database");
        return false;
    }
    return true;
}

function store_upload($key, $filename, $tmp_file, $note) {
    error_log("Storing upload with key $key, filename $filename, tmp file $tmp_file");
    if (!is_uploaded_file($tmp_file)) {
        error_log("not uploaded file?");
        return false;
    }
    if (null === ($db = get_connection())) {
        return false;
    }
    $st = $db->prepare("select num_files from upload_link where hash = ?;");
    if (!$st->execute(array($key))) {
        error_log("Unable to execute query.");
        return "Internal error fetching data";
    }
    $res = $st->fetchAll();
    if (count($res) == 0) {
        error_log("Unable to find upload link with key $key.");
        return false;
    }
    if (strlen($note) > 1000) {
        $note = substr($note, 0, 1000);
        $note .= " (server-trimmed)";
    }
    $st = $db->prepare("insert into uploads (upload_id, filename, note) values (?, ?, ?);");
    if (!$st->execute(array($key, $filename, $note))) {
        error_log("Unable to insert into uploads.");
        return 'Error storing uploaded file metadata';
    }
    $id = $db->lastInsertId();
    $new_filename = "/data/$key-upload-$id";
    if (!move_uploaded_file($tmp_file, $new_filename)) {
        error_log("Error saving uploaded file.");
        $st = $db->prepare("delete from uploads where rowid = ?;");
        $st->execute(array($id));
        error_log("deleted upload record with id $id");
        return 'Internal error saving uploaded file.';
    }
    return true;
}

function log_download($db, $key) {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		//ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		//ip pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
    $st = $db->prepare("insert into download_log (hash, ipaddr) values (?,?);");
    if (!$st->execute(array($key, $ip))) {
        error_log("Error inserting into download log for hash " . $key . ".");
    }
}

function serve_available_download($key) {
    if (null === ($db = get_connection())) {
        return null;
    }
    $st = $db->prepare("select filename from downloads where hash = ?;");
    if (!$st->execute(array($key))) {
        error_log("error executing query for key $key to serve available download.");
        return false;
    }
    $result = $st->fetchAll();
    if (count($result) !== 1) {
        error_log("Requesting download key $key, count is " . count($result));
        return false;
    }
    $filename = '/data/' . $result[0]['filename'];
    if (!file_exists($filename)) {
        error_log("requested file, $filename,  doesn't exist.");
        return false;
    }
    error_log("sending file $filename encoded is " . urlencode($result[0]['filename']));
    header('Content-Type: application/octet-stream');
    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename="'.urlencode($result[0]['filename']).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
    log_download($db, $key);
    return true;
}

/* We rewrite the available_downloads file skipping the entry we want to delete*/
function delete_download_link($key) {
    error_log("Deleting download link $key.");
    if (null === ($db = get_connection())) {
        return false;
    }
    $st = $db->prepare("select filename from downloads where hash = ?;");
    if (!$st->execute(array($key))) {
        error_log("Couldn't find record.");
        return false;
    }
    $res = $st->fetchAll();
    if (count($res) == 0) {
        error_log("No record exists for key $key");
        return false;
    }
    $st = $db->prepare("delete from downloads where hash = ?;");
    if (!$st->execute(array($key))) {
        error_log("Unable to execute delete for key $key");
        return false;
    }
    $file = $res[0]['filename'];
    $file = "/data/$file";
    if (!file_exists($file)) {
        error_log("Unable to find file, $file");
        return false;
    }
    unlink($file);
    return true;
}
?>
